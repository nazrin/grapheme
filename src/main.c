///
// @module grapheme
// @license MPL-2.0
// @author nazrin

#include <lua.h>
#include <lauxlib.h>
#include <grapheme.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#endif

struct Buffer{
	size_t size;
	size_t maxSize;
	char* data;
} typedef Buffer;
static int bufferInit(struct Buffer* b, size_t maxSize){
	if(maxSize == 0)
		maxSize = 1; // 0 makes sense as a valid size but the way the buffer is doubled in size until it fits can't handle 0 so we change it to 1
	b->data = malloc(maxSize);
	if(!b->data)
		return -1;
	b->size = 0;
	b->maxSize = maxSize;
	return 0;
}
static void bufferFree(struct Buffer* b){
	free(b->data);
}
static int bufferAddstr(struct Buffer* b, const char* str, size_t size){
	if((b->maxSize - b->size) < size){
		while((b->maxSize - b->size) < size){
			if(b->maxSize > SIZE_MAX/2) // Overflow
				goto fail;
			b->maxSize = b->maxSize * 2;
		}
		char* newData = realloc(b->data, b->maxSize);
		if(!newData)
			goto fail;
		b->data = newData;
	}
	memcpy(b->data + b->size, str, size);
	b->size += size;
	return 0;
fail:
	bufferFree(b);
	return -1;
}

enum Cases  { UPPER, LOWER, TITLE };
enum Breaks { CHAR, SENT, WORD };

static void appendToTable(lua_State* L, const struct luaL_Reg* lib){
	for(int i = 0; lib[i].func; i++){
		lua_pushcfunction(L, lib[i].func);
		lua_setfield(L, -2, lib[i].name);
	}
}
static int utf8case(lua_State* L, enum Cases cas){
	size_t slen, nlen;
	const char* s = luaL_checklstring(L, 1, &slen);
	if(slen == 0)
		return 1;
	char* n = malloc(slen * 2);
	if(!n)
		luaL_error(L, "Not enough memory!");
	switch(cas){
		case LOWER: nlen = grapheme_to_lowercase_utf8(s, slen, n, slen * 2); break;
		case UPPER: nlen = grapheme_to_uppercase_utf8(s, slen, n, slen * 2); break;
		case TITLE: nlen = grapheme_to_titlecase_utf8(s, slen, n, slen * 2); break;
	}
	if(nlen < 1)
		goto end;
	lua_pushlstring(L, n, nlen);
end:
	free(n);
	return 1;
}
/// to lowercase
// @function lower
// @string str UTF-8
// @return str UTF 8
static int lib_lower(lua_State* L){ return utf8case(L, LOWER); }
/// TO UPPERCASE
// @function upper
// @string str UTF-8
// @return str UTF 8
static int lib_upper(lua_State* L){ return utf8case(L, UPPER); }
/// To Titlecase
// @function title
// @string str UTF-8
// @return str UTF 8
static int lib_title(lua_State* L){ return utf8case(L, TITLE); }

static int utf8isCase(lua_State* L, enum Cases cas){
	size_t slen;
	const char* s = luaL_checklstring(L, 1, &slen);
	bool b = false;
	if(slen){
		switch(cas){
			case LOWER: b = grapheme_is_lowercase_utf8(s, slen, NULL); break;
			case UPPER: b = grapheme_is_uppercase_utf8(s, slen, NULL); break;
			case TITLE: b = grapheme_is_titlecase_utf8(s, slen, NULL); break;
		}
	}
	lua_pushboolean(L, b);
	return 1;
}
/// is lowercase?
// @function isLower
// @string str UTF-8
// @return bool
static int lib_islower(lua_State* L){ return utf8isCase(L, LOWER); }
/// IS UPPERCASE?
// @function isUpper
// @string str UTF-8
// @return bool
static int lib_isupper(lua_State* L){ return utf8isCase(L, UPPER); }
/// Is Titlecase?
// @function isTitle
// @string str UTF-8
// @return bool
static int lib_istitle(lua_State* L){ return utf8isCase(L, TITLE); }

/// Encodes a UTF-32 string or table of codepoints into a UTF-8 string
// @function encode
// @param stringOrTable UTF-32 string or table
// @return str UTF-8
static int lib_encode(lua_State* L){
	Buffer b;
	if(bufferInit(&b, 1))
		luaL_error(L, "Not enough memory!");
	char s[4];
	switch(lua_type(L, 1)){
		case LUA_TTABLE: (void)0;
			lua_rawgeti(L, 1, 1);
			for(size_t i = 2; lua_type(L, -1) == LUA_TNUMBER; i++){
				uint_least32_t c = lua_tointeger(L, -1);
				int r = grapheme_encode_utf8(c, s, 4);
				if(bufferAddstr(&b, s, r))
					luaL_error(L, "Not enough memory!");
				lua_pop(L, 1);
				lua_rawgeti(L, 1, i);
			}
			if(!lua_isnoneornil(L, -1)){
				bufferFree(&b);
				luaL_argerror(L, 1, "Non-number in table");
			}
			break;
		case LUA_TSTRING: (void)0;
			size_t slen;
			const char* ss = lua_tolstring(L, 1, &slen);
			if(slen % 4)
				luaL_error(L, "Invalid string");
			for(size_t i = 0; i < slen; i += 4){
				uint_least32_t c = *((uint32_t*)&ss[i]);
				c = htonl(c);
				int r = grapheme_encode_utf8(c, s, 4);
				if(bufferAddstr(&b, s, r))
					luaL_error(L, "Not enough memory!");
			}
			break;
		default:
			bufferFree(&b);
			luaL_argerror(L, 1, "Need a table or string");
	}
	lua_pushlstring(L, b.data, b.size);
	bufferFree(&b);
	return 1;
}
/// Decodes a UTF-8 string into a UTF-32 string or a table of codepoints
// @function decode
// @string str UTF-8
// @return stringOrTable UTF-32 string or table
static int lib_decode(lua_State* L){
	size_t slen, off = 0;
	const char* s = luaL_checklstring(L, 1, &slen);
	const char* tabStr = luaL_optstring(L, 2, "t");
	const bool tab = strcmp(tabStr, "t") == 0;
	luaL_argcheck(L, tab || strcmp(tabStr, "s") == 0, 2, "Needs to be 's' or 't'");
	uint_least32_t c;
	if(tab){
		lua_newtable(L);
		size_t i = 1;
		while(off < slen){
			off += grapheme_decode_utf8(s + off, slen - off, &c);
			lua_pushinteger(L, c);
			lua_rawseti(L, -2, i++);
		}
	} else {
		Buffer b;
		if(bufferInit(&b, slen))
			luaL_error(L, "Not enough memory!");
		while(off < slen){
			off += grapheme_decode_utf8(s + off, slen - off, &c);
			c = htonl(c);
			if(bufferAddstr(&b, (const char*)&c, sizeof(c)))
				luaL_error(L, "Not enough memory!");
		}
		lua_pushlstring(L, b.data, b.size);
		bufferFree(&b);
	}
	return 1;
}

static int utf8Break(lua_State* L, enum Breaks bre){
	size_t slen, off = 0;
	const char* s = luaL_checklstring(L, 1, &slen);
	lua_newtable(L);
	size_t r, i = 1;
	while(off < slen){
		switch(bre){
			case CHAR: r = grapheme_next_character_break_utf8(s + off, slen - off); break;
			case SENT: r = grapheme_next_sentence_break_utf8(s + off, slen - off); break;
			case WORD: r = grapheme_next_word_break_utf8(s + off, slen - off); break;
		}
		if(r == 0)
			break;
		lua_pushlstring(L, s + off, r);
		off += r;
		lua_rawseti(L, -2, i++);
	}
	return 1;
}
/// Splits a UTF-8 string into a table of separate chars
// @function chars
// @string str UTF-8
// @return table UTF-8 strings
static int lib_chars(lua_State* L){ return utf8Break(L, CHAR); }
/// Splits a UTF-8 string into a table of separate sentences
// @function sentences
// @string str UTF-8
// @return table UTF-8 strings
static int lib_sentences(lua_State* L){ return utf8Break(L, SENT); }
/// Splits a UTF-8 string into a table of separate words
// @function words
// @string str UTF-8
// @return table UTF-8 strings
static int lib_words(lua_State* L){ return utf8Break(L, WORD); }

/// Number of codepoints
// @function len
// @string str UTF-8
// @return int
static int lib_len(lua_State* L){
	size_t slen, l, off = 0, prev = 0;
	const char* s = luaL_checklstring(L, 1, &slen);
	for(l = 0; off < slen; l++){
		off += grapheme_next_character_break_utf8(s + off, slen - off);
		if(off == prev)
			break;
		prev = off;
	}
	lua_pushinteger(L, l);
	return 1;
}

static const struct luaL_Reg graphemeLib[] = {
	{ "lower",     lib_lower },
	{ "upper",     lib_upper },
	{ "title",     lib_title },
	{ "isLower",   lib_islower },
	{ "isUpper",   lib_isupper },
	{ "isTitle",   lib_istitle },
	{ "encode",    lib_encode },
	{ "decode",    lib_decode },
	{ "chars",     lib_chars },
	{ "sentences", lib_sentences },
	{ "words",     lib_words },
	{ "len",       lib_len },
	{ NULL, NULL }
};

int luaopen_grapheme(lua_State* L){
	lua_newtable(L);
	appendToTable(L, graphemeLib);

	lua_pushstring(L, "MPL-2.0");
	lua_setfield(L, -2, "_LICENSE");

	return 1;
}

