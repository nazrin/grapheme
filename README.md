# Lua Grapheme

Lua binding to [libgrapheme](https://libs.suckless.org/libgrapheme/ "libgrapheme"), a light and fast Unicode/UTF-8 library

The library is included automatically as a submodule and will be statically linked. No external dependencies

## Building

```sh
git clone --recursive https://codeberg.org/nazrin/grapheme 
cd grapheme
make LUA_VERSION=5.X
```

## Installing with Luarocks

Install: `luarocks install grapheme --lua-version=5.X`

Link: [luarocks.org/modules/nazrin/grapheme](https://luarocks.org/modules/nazrin/grapheme)

## Example

```lua
local gr = require("grapheme")

print(gr.lower("ПРИВЕТ")) --> привет

for i,x in ipairs(gr.words("Sjáum þetta.")) do
	print(x) --> "Sjáum", " ", "þetta", "."
end
```

## API

### Conversion

* `utf8 gr.lower(utf8 str)` To lowercase
* `utf8 gr.upper(utf8 str)` To UPPERCASE
* `utf8 gr.title(utf8 str)` To Titlecase

### Querying

* `bool gr.isLower(utf8 str)`
* `bool gr.isUpper(utf8 str)`
* `bool gr.isTitle(utf8 str)`
* `int gr.len(utf8 str)` Number of codepoints

### Splitting

* `table gr.chars(utf8 str)` `{ 'T', 'o', ' ', 'c', 'h', 'a', 'r', 's', '.' }`
* `table gr.sentences(utf8 str)` `{ 'To sentences.' }`
* `table gr.words(utf8 str)` `{ 'To', ' ', 'words', '.' }`

### Encoding

* `utf8 gr.encode(string|table val)`
Encodes a UTF-32 (NBO) string or table of codepoints into a UTF-8 string
* `string|table gr.decode(utf8 str, ["s"|"t"])`
Decodes a UTF-8 string into a table of codepoints `"t"` (default) or UTF-32 (NBO) string `"s"`

## Credits

* Laslo Hunhold, libgrapheme

