PROGRAM = grapheme.so

CC     = cc
CFLAGS = -Wall -Wpedantic -std=c99 -Ilib/libgrapheme/ -Os
LFLAGS = -shared -l:libgrapheme.a -Llib/libgrapheme/
DIR    = src

# CFLAGS += -g
# CFLAGS += -fprofile-arcs -ftest-coverage
# LFLAGS += -fprofile-arcs

LUA_VERSION = jit

ifeq ($(LUA_VERSION), jit)
CFLAGS += `pkg-config --cflags luajit || echo -I/usr/include/lua{,5.1}`
else ifeq ($(LUA_VERSION), 5.1)
CFLAGS += `pkg-config --cflags lua5.1 --silence-errors`
else ifeq ($(LUA_VERSION), 5.2)
CFLAGS += `pkg-config --cflags lua5.2 --silence-errors`
else ifeq ($(LUA_VERSION), 5.3)
CFLAGS += `pkg-config --cflags lua5.3 --silence-errors`
else ifeq ($(LUA_VERSION), 5.4)
CFLAGS += `pkg-config --cflags lua5.4 --silence-errors`
else ifeq ($(LUA_VERSION), rock)
else
$(error Invalid Lua version $(LUA_VERSION))
endif

SRC = $(wildcard $(DIR)/*.c)
OBJ = $(SRC:.c=.o)

$(PROGRAM): $(OBJ) lib/libgrapheme/libgrapheme.a
	$(CC) $(OBJ) $(LFLAGS) -o $(PROGRAM)

%.o: %.c makefile
	$(CC) -c $(CFLAGS) $< -o $@

lib/libgrapheme/libgrapheme.a:
	cd lib/libgrapheme && make

.PHONY: clean
clean:
	rm -fv -- $(OBJ) $(PROGRAM)

