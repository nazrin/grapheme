package.cpath = "./?.so"
local gr = require("grapheme")

local function cmp(a, b)
	if type(a) == "table" then
		for i,va in ipairs(a) do cmp(va, b[i]) end
		for i,vb in ipairs(b) do cmp(vb, a[i]) end
	else
		assert(a == b, ("%q, %q"):format(a, b))
	end
end
local function checkFail(func, ...)
	local ok, err = pcall(func, ...)
	assert(not ok)
end

local strs = { "ÞETTA ER FRÁBÆRT", "так и есть", "Frogs don't live in the sea. :)", "", "a", "Ω" }
table.insert(strs, ("long one"):rep(8000))

for s,str in ipairs(strs) do
	local l, u, t = gr.lower(str), gr.upper(str), gr.title(str)
	if #str > 0 then
		assert(l ~= u)
		assert(t ~= l)
		assert(gr.isLower(l))
		assert(gr.isUpper(u))
		assert(gr.isTitle(t))
	end
	assert(gr.encode(gr.decode(t)) == t)
	assert(gr.encode(gr.decode(t, "s")) == t)
end

assert(gr.title("í") == "Í")
assert(gr.title("Æ") == "Æ")

assert(not gr.isLower(""))
assert(not gr.isUpper(""))
assert(not gr.isTitle(""))
assert(gr.isLower("アップル"))
assert(gr.isUpper("アップル"))
assert(gr.isTitle("アップル"))

assert(gr.isLower("l"))
assert(gr.isUpper("Ж"))
assert(gr.isTitle("Í."))

cmp(gr.decode(" ", "t"), { 32 })
cmp(gr.decode("ǽ", "t"), { 509 })

assert(gr.encode(gr.decode("sæll")) == "sæll" )
assert(gr.encode(gr.decode("привет")) == "привет" )

assert(gr.encode(gr.decode("Þú123")) == "Þú123")
assert(gr.encode(gr.decode("Þú123", "s")) == "Þú123")

cmp(gr.sentences("Привет 123."), { "Привет 123." })
cmp(gr.words("Sæll 123."), { "Sæll", " ", "123", "." })
cmp(gr.chars("Já 123."), { 'J', '\195\161', ' ', '1', '2', '3', '.' })

assert(gr.len("") == 0)
assert(gr.len("þetta") == 5)
assert(gr.len("привет") == 6)
assert(gr.len("アップル") == 4)

assert(gr.upper("أهلا") == "أهلا")
assert(gr.upper("Χαίρετε") == "ΧΑΊΡΕΤΕ")
assert(gr.upper("გამარჯობა") == "ᲒᲐᲛᲐᲠᲯᲝᲑᲐ")

local function shouldFail(f, ...)
	local ok, err = pcall(f, ...)
	assert(not ok)
end

shouldFail(gr.decode, "", "")
shouldFail(gr.decode, "", ".")
shouldFail(gr.encode, ".")

for k,v in pairs(gr) do
	if type(v) == "function" then
		shouldFail(v)
		shouldFail(v, nil)
		shouldFail(v, io.stdout)
		v("")
		if k ~= "encode" then
			v("\195")
			v("\000")
			v("a\195")
			v("\195a")
			v("a\195a")
		end
	end
end

-- Malformed characters
cmp(gr.chars("\195"), { })
cmp(gr.chars("a\195"), { "a" })
cmp(gr.chars("\195a"), { "\195", "a" })

cmp(gr.words("\195"), { })
cmp(gr.words("a\195"), { "a" })
cmp(gr.words("\195a"), { "\195", "a" })

cmp(gr.sentences("\195"), { })
cmp(gr.sentences("a\195"), { "a" })
cmp(gr.sentences("\195a"), { "\195a" })

assert(gr.len("\195") == 0)
assert(gr.len("a\195") == 1)
assert(gr.len("\195a") == 2)

